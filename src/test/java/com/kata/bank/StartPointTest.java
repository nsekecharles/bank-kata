package com.kata.bank;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StartPointTest {

    @Test
    void allIsOK() {
        Assertions.assertEquals("Jupiter is well install", "Jupiter is well install");
        org.assertj.core.api.Assertions.assertThat("AssertJ is install").isEqualTo("AssertJ is install");
    }
}
