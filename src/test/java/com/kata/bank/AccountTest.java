package com.kata.bank;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mockStatic;

@ExtendWith(MockitoExtension.class)
public class AccountTest {

    @Test
    void should_print_statement() {
        String actual = new Account().printStatement();

        Assertions.assertThat(actual).isEqualTo("Date        Amount  Balance\n");
    }

    @Test
    void should_add_to_account_balance() {
        final Account account = new Account();

        final MockedStatic<LocalDate> mock = mockStatic(LocalDate.class);
        mock.when(LocalDate::now).thenReturn(LocalDate.of(2020, 1, 1));

        //  Mockito.when(LocalDate.now()).thenReturn(LocalDate.of(2020, 1, 1));

        /**try (var mocked = Mockito.mockStatic(LocalDate.class)) {
         when(mocked.now()).thenReturn(LocalDate.of(2020, 1, 1));
         account.deposit(500);
         }**/

        String actual = account.printStatement();
        Assertions.assertThat(actual).isEqualTo("Date        Amount  Balance\n01/01/2020 +500 500");
    }
/*
    @Test
    void should_add_to_account_when_doing_deposit_balance() {
        final Account account = new Account();

        account.deposit(LocalDate.of(2020, 1, 1), 500);
        account.deposit(LocalDate.of(2020, 5, 3), 500);
        account.deposit(LocalDate.of(2020, 10, 23), 500);

        String actual = account.printStatement();
        Assertions.assertThat(actual).isEqualTo("Date        Amount  Balance\n01/01/2020 +500 500\n03/05/2020 +500 1000\n23/10/2020 +500 1500");
    }

    @Test
    void should_withdraw_to_account_balance() {
        final Account account = new Account();

        account.withdraw(LocalDate.of(2020, 1, 1), 500);

        String actual = account.printStatement();
        Assertions.assertThat(actual).isEqualTo("Date        Amount  Balance\n01/01/2020 -500 -500");
    }

    @Test
    void should_withdraw_and_deposit_to_account_balance() {
        final Account account = new Account();

        account.withdraw(LocalDate.of(2020, 1, 1), 500);
        account.deposit(LocalDate.of(2020, 5, 3), 100);

        String actual = account.printStatement();
        Assertions.assertThat(actual).isEqualTo("Date        Amount  Balance\n01/01/2020 -500 -500\n03/05/2020 +100 -400");
    }
**/
}
