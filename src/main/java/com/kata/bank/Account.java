package com.kata.bank;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Account {
    private Integer balance = 0;
    private final List<Operation> operations = new ArrayList<>();
    private static final String HEADER = "Date        Amount  Balance\n";

    public String printStatement() {
        return HEADER + operations.stream().map(Operation::toString).collect(Collectors.joining("\n"));
    }

    public void deposit(int amount) {
        balance += amount;
        operations.add(new Operation(LocalDate.now(), "+", amount, balance));
    }

    public void withdraw(LocalDate date, int amount) {
        balance -= amount;
        operations.add(new Operation(date, "-", amount, balance));
    }
}
