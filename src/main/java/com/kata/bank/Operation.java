package com.kata.bank;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Operation {
    private Integer balance;
    private int amount;
    private String sign;
    private LocalDate date;

    public Operation(LocalDate date, String sign, int amount, Integer balance) {
        this.amount = amount;
        this.balance = balance;
        this.sign = sign;
        this.date = date;
    }

    @Override
    public String toString() {
        return date.format(DateTimeFormatter.ofPattern("dd/MM/YYYY")) + " " + sign + amount + " " + balance;
    }
}
